using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class PlayableTime : BasicPlayableBehaviour
{
    public AnimationCurve Curve;
    public float Scale = 1;


    public override void OnGraphStart(Playable playable)
    {

    }

    public override void OnGraphStop(Playable playable)
    {
        Time.timeScale = 1;
    }

    public override void OnBehaviourPlay(Playable playable, FrameData info)
    {
    }

    public override void OnBehaviourPause(Playable playable, FrameData info)
    {
        
    }

    public override void PrepareFrame(Playable playable, FrameData info)
    {
        //curTime += info.deltaTime;
        Time.timeScale = Scale;
    }

  
}
