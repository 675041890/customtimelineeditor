using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class LightControlMixerBehaviour : PlayableBehaviour
{
    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
        Light light = playerData as Light;
        float finalIntensity = 0f;
        Color finalColor = Color.black;

        if (!light)
            return;

        int inputCount = playable.GetInputCount();

        for (int i = 0; i < inputCount; i++)
        {
            float inputWeight = playable.GetInputWeight(i);Debug.Log($"i : {i}, weight: {inputWeight}");
            ScriptPlayable <LightControlBehaviour> inputPlayable = (ScriptPlayable<LightControlBehaviour>)playable.GetInput(i);
            LightControlBehaviour input = inputPlayable.GetBehaviour();

            finalIntensity += input.intensity * inputWeight;
            finalColor += input.color * inputWeight;
        }

        light.intensity = finalIntensity;
        light.color = finalColor;
    }
}
