using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class LightControlAsset : PlayableAsset
{
    public LightControlBehaviour Template;

    ////public ExposedReference<Light> Light;
    //public Color Color = Color.white;
    //public float intensity = 1f;


    public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<LightControlBehaviour>.Create(graph,Template);
        //var lightControlBehaviour = playable.GetBehaviour();
        ////lightControlBehaviour.Light = Light.Resolve(graph.GetResolver());
        //lightControlBehaviour.color = Color;
        //lightControlBehaviour.intensity = intensity;
        return playable;

    }


}
