using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

[System.Serializable]
public class LightControlBehaviour : PlayableBehaviour
{
    //public Light Light = null;
    public Color color = Color.white;
    public float intensity = 1f;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="playable"></param>
    /// <param name="info"></param>
    /// <param name="playerData">track's Bound Object</param>
    //public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    //{
    //    Light light = playerData as Light;

    //    if (light != null)
    //    {
    //        light.color = color;
    //        light.intensity = intensity;
    //    }
    //}

}
