using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Playables;

[RequireComponent(typeof(Animator))]
public class MixAnimationSample : MonoBehaviour
{
    public AnimationClip Clip1;
    public AnimationClip Clip2;
    public float weight;
    PlayableGraph playableGraph;
    AnimationMixerPlayable mixerPlayable;
    // Start is called before the first frame update
    void Start()
    {
        playableGraph = PlayableGraph.Create();
        mixerPlayable = AnimationMixerPlayable.Create(playableGraph,2);
        var clipPlayable1 = AnimationClipPlayable.Create(playableGraph,Clip1);
        var clipPlayable2 = AnimationClipPlayable.Create(playableGraph,Clip2);

        playableGraph.Connect(clipPlayable1,0, mixerPlayable,0);
        playableGraph.Connect(clipPlayable2,0, mixerPlayable,1);

        var playableOutput = AnimationPlayableOutput.Create(playableGraph, "mixer", GetComponent<Animator>());
        playableOutput.SetSourcePlayable(mixerPlayable);
        playableGraph.Play();
    }

    // Update is called once per frame
    void Update()
    {
        weight = Mathf.Clamp01(weight);
        mixerPlayable.SetInputWeight(0, weight);
        mixerPlayable.SetInputWeight(1, 1 - weight);
    }

    void OnDisable()
    {
        playableGraph.Destroy();
    }
}
