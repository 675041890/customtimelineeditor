using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Playables;

[RequireComponent(typeof(Animator))]
public class RuntimeControllerSample : MonoBehaviour
{
    public AnimationClip clip;

    public RuntimeAnimatorController controller;

    public float weight;

    PlayableGraph playableGraph;

    AnimationMixerPlayable mixerPlayable;
    // Start is called before the first frame update
    void Start()
    {
        playableGraph = PlayableGraph.Create();
        var playableOutput = AnimationPlayableOutput.Create(playableGraph, "runtimeAnimatorCol", GetComponent<Animator>());
        mixerPlayable = AnimationMixerPlayable.Create(playableGraph, 2);
        playableOutput.SetSourcePlayable(mixerPlayable);

        var playableRuntimeCon = AnimatorControllerPlayable.Create(playableGraph, controller);
        var clipPlayable = AnimationClipPlayable.Create(playableGraph, clip);

        playableGraph.Connect(playableRuntimeCon, 0, mixerPlayable, 0);
        playableGraph.Connect(clipPlayable, 0, mixerPlayable, 1);

        playableGraph.Play();
        
    }

    // Update is called once per frame
    void Update()
    {
        weight = Mathf.Clamp01(weight);
        mixerPlayable.SetInputWeight(0, weight);
        mixerPlayable.SetInputWeight(1, 1-weight);
    }

    void OnDisable()
    {
        playableGraph.Destroy();
    }
}
