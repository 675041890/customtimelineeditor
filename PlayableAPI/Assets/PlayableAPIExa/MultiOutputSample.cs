using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Audio;
using UnityEngine.Playables;

[RequireComponent(typeof(Animator))]
public class MultiOutputSample : MonoBehaviour
{
    public AnimationClip AnimationClip;
    public AudioClip audioClip;
    PlayableGraph playableGraph;

    // Start is called before the first frame update
    void Start()
    {
        playableGraph = PlayableGraph.Create();
        var animationOutput = AnimationPlayableOutput.Create(playableGraph, "Animation", GetComponent<Animator>());
        var audioOutput = AudioPlayableOutput.Create(playableGraph, "Audio", GetComponent<AudioSource>());

        var animationClipPlayable = AnimationClipPlayable.Create(playableGraph, AnimationClip);
        var audioClipPlayable = AudioClipPlayable.Create(playableGraph, audioClip,true);

        animationOutput.SetSourcePlayable(animationClipPlayable);
        audioOutput.SetSourcePlayable(audioClipPlayable);

        playableGraph.Play();
    }

    // Update is called once per frame
    void OnDisable()
    {
        playableGraph.Destroy();
    }
}
