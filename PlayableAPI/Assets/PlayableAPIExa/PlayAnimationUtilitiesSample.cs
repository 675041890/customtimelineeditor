using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

[RequireComponent(typeof(Animator))]
public class PlayAnimationUtilitiesSample : MonoBehaviour
{
    public AnimationClip Clip;
    PlayableGraph playableGraph;
    // Start is called before the first frame update
    void Start()
    {
        AnimationPlayableUtilities.PlayClip(GetComponent<Animator>(),Clip,out playableGraph);
    }

    // Update is called once per frame
    void OnDisable()
    {
        playableGraph.Destroy();
    }
}
