using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Playables;

[RequireComponent(typeof(Animator))]
public class PlayAnimationSample : MonoBehaviour
{
    public AnimationClip Clip;
    PlayableGraph playableGraph;
    // Start is called before the first frame update
    void Start()
    {
        playableGraph = PlayableGraph.Create("SimpleAnimation");
        playableGraph.SetTimeUpdateMode(DirectorUpdateMode.GameTime); // 设置回放时的播放方式
        var playableOutput = AnimationPlayableOutput.Create(playableGraph, "Animation",GetComponent<Animator>());
        var clipPlayable = AnimationClipPlayable.Create(playableGraph,Clip);
        playableOutput.SetSourcePlayable(clipPlayable);
        playableGraph.Play();

    }

    // Update is called once per frame
    void OnDisable()
    {
        playableGraph.Destroy();
    }
}
