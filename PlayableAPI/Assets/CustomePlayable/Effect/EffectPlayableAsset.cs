using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class EffectPlayableAsset : PlayableAsset,ITimelineClipAsset
{

    public EffectPlayableBehaviour EffectAsset;

    public ExposedReference<Transform> Parent;
    public ClipCaps clipCaps { get; set; }

    public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
    {
        EffectAsset.Parent = Parent.Resolve(graph.GetResolver());
        var playable = ScriptPlayable<EffectPlayableBehaviour>.Create(graph,EffectAsset);
        
        return playable;

    }

}
