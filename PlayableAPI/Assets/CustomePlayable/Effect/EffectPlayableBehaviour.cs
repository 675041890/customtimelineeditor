using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;


// OnPlayableCreate  -->  OnGraphStart  -->  （开始时间点)OnBehaviourPlay -->  PrepareFrame(每帧执行)  -->  ProcessFrame(每帧执行)  -->  OnbehaviourPause(超过Clip时间后)  -->  OnGraphStop  -->  PnPlayableDestroy
[Serializable]
public class EffectPlayableBehaviour : PlayableBehaviour
{
    public Vector3 Position;
    public Vector3 Rotation;
    public bool IsLoop;
    public ParticleSystem Particle;
    [NonSerialized]
    public Transform Parent;
    private ParticleSystem refrence;

    public override void OnGraphStop(Playable playable)
    {
       
    }

    public override void OnBehaviourPause(Playable playable, FrameData info)
    {
        if (refrence != null)
        {
            GameObject.DestroyImmediate(refrence.gameObject);
            refrence = null;
        }
    }

    // 1.
    public override void OnPlayableCreate(Playable playable)
    {
        
    }

    public override void OnPlayableDestroy(Playable playable)
    {
        if (refrence != null)
        {
            GameObject.DestroyImmediate(refrence.gameObject);
            refrence = null;
        }
    }

    //2.
    public override void OnGraphStart(Playable playable)
    {

    }

    public override void OnBehaviourPlay(Playable playable, FrameData info)
    {
        if (Particle && !refrence)
        {
            refrence = GameObject.Instantiate(Particle);
        }
        if (Parent != null)
        {
            refrence.transform.SetParent(Parent.transform);
        }
        refrence.transform.localPosition = Position;
        refrence.transform.localEulerAngles = Rotation;
        refrence.loop = IsLoop;


        refrence.Play(true);      
    }

    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {

    }

    public override void PrepareData(Playable playable, FrameData info)
    {
        
    }
    public override void PrepareFrame(Playable playable, FrameData info)
    {
        
    }
}
