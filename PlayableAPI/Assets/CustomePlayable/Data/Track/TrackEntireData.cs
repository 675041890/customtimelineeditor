using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class TrackEntireData<T> : ISetTrackAllData where T : ISetClipAllData, new ()
{
    public BaseTrackData BaseTrackData;
    /// <summary>
    /// 轨道上的Clip信息
    /// </summary>
    public List<T> ClipsOfTrack;

    //public Dictionary<string, T> ClipsOfTrack; // Unity无法序列化

    public void SetAllData(PlayableBinding playableBinding, PlayableDirector playDirector)
    {
        BaseTrackData = BaseTrackData.SetBaseData(playableBinding, playDirector);

        //ClipsOfTrack = new Dictionary<string, T>();
        ClipsOfTrack = new List<T>();
        TrackAsset trackAsset = playableBinding.sourceObject as TrackAsset;
        foreach (TimelineClip clip in trackAsset.GetClips())
        {
            T shotData = new T();
            shotData.SetAllData(clip, BaseTrackData.TrackName, playDirector);
            //ClipsOfTrack.Add(clip.displayName,shotData);
            ClipsOfTrack.Add(shotData);
        }
    }

}


public class EventTrackData : TrackEntireData<EventData>
{


}

public class EventData : ISetClipAllData
{
    public void SetAllData(TimelineClip clip, string attachtrack, PlayableDirector playDirector)
    {
        throw new System.NotImplementedException();
    }
}

public class Timelines
{
    EventTrackData eventTrackData = new EventTrackData();

}