using System;
using System.Collections.Generic;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class TimeTrackData : TrackEntireData<PlayableTimeData>
{
    public PlayableTimeData GetClipData(string clipName)
    {
        foreach (var clip in ClipsOfTrack)
        {
            if (clip.ClipDisplayName == clipName)
            {
                return clip;
            }

        }
        return null;
    }
}
