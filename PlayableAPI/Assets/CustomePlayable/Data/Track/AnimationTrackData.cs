using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class AnimationTrackData : TrackEntireData<AnimationClipData>
{
    public AnimationClipData GetClipData(string clipName)
    {
        foreach (var clip in ClipsOfTrack)
        {
            if (clip.ClipDisplayName == clipName)
            {
                return clip;
            }

        }
        return null;
    }
}

