using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

[Serializable]
public class BaseTrackData
{
    /// <summary>
    /// 轨道名称
    /// </summary>
    public string TrackName;

    /// <summary>
    /// 轨道绑定目标名称
    /// </summary>
    public string TrackBindingName;

    public static BaseTrackData SetBaseData(PlayableBinding playableBinding, PlayableDirector playDirector)
    {
        UnityEngine.Object bindingKey = playableBinding.sourceObject;
        // 获取Binding.GameObject
        var bindGo = playDirector.GetGenericBinding(bindingKey);
        BaseTrackData baseData = new BaseTrackData();
        if (bindGo != null)
            baseData.TrackBindingName = bindGo.name;
        baseData.TrackName = playableBinding.streamName;
        return baseData;
    }
}
