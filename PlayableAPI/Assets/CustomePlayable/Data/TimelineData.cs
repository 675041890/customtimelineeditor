using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

/// <summary>
/// PlayableAsset文件数据类 
/// </summary>
[Serializable]
public class TimelineData 
{
    public List<AnimationTrackData>     AnimationTrackDatas;
    public List<AudioTrackData>         AudioTrackDatas;
    public List<CinemachineTrackData>   CinemachineTrackDatas;
    public List<EffectTrackData>        EffectTrackDatas;
    public List<TimeTrackData>          TimeTrackDatas;

    public TimelineData()
    {
        AnimationTrackDatas = new List<AnimationTrackData>();
        AudioTrackDatas = new List<AudioTrackData>();
        CinemachineTrackDatas = new List<CinemachineTrackData>();
        EffectTrackDatas = new List<EffectTrackData>();
        TimeTrackDatas = new List<TimeTrackData>();
    }

    private T getTrackEntireData<T>(PlayableBinding playableBinding, PlayableDirector playableDirector) where T : ISetTrackAllData, new()
    {
        T trackData = new T();
        trackData.SetAllData(playableBinding, playableDirector);
        return trackData;
    }
    
    public void SetTimelineData(PlayableBinding playableBinding, PlayableDirector playableDirector, Type type)
    {

        if (type == typeof(AnimationTrack))
        {
            var data = getTrackEntireData<AnimationTrackData>(playableBinding, playableDirector);
            AnimationTrackDatas.Add(data);
        }
        if (type == typeof(AudioTrack))
        {
            var data = getTrackEntireData<AudioTrackData>(playableBinding, playableDirector);
            AudioTrackDatas.Add(data);
        }
        if (type == typeof(CinemachineTrack))
        {
            var data = getTrackEntireData<CinemachineTrackData>(playableBinding, playableDirector);
            CinemachineTrackDatas.Add(data);
        }
        if (type == typeof(EffectTrack))
        {
            var data = getTrackEntireData<EffectTrackData>(playableBinding, playableDirector);
            EffectTrackDatas.Add(data);
        }
        if (type == typeof(TimeTrack))
        {
            var data = getTrackEntireData<TimeTrackData>(playableBinding, playableDirector);
            TimeTrackDatas.Add(data);
        }
    }

    public AnimationClipData GetAnimation(string clipName)
    {
        foreach (var item in AnimationTrackDatas)
        {
            foreach (var clip in item.ClipsOfTrack)
            {
                if (clip.ClipDisplayName == clipName)
                {
                    return clip;
                }
            }
        }
        return null;
    }

    public AudioClipData GetAudio(string clipName)
    {
        foreach (var item in AudioTrackDatas)
        {
            foreach (var clip in item.ClipsOfTrack)
            {
                if (clip.ClipDisplayName == clipName)
                {
                    return clip;
                }
            }
        }
        return null;
    }

    public CinemachineShotData GetCinemachineShot(string clipName)
    {
        foreach (var item in CinemachineTrackDatas)
        {
            foreach (var clip in item.ClipsOfTrack)
            {
                if (clip.ClipDisplayName == clipName)
                {
                    return clip;
                }
            }
        }
        return null;
    }

    public EffectClipData GetEffect(string clipName)
    {
        foreach (var item in EffectTrackDatas)
        {
            foreach (var clip in item.ClipsOfTrack)
            {
                if (clip.ClipDisplayName == clipName)
                {
                    return clip;
                }
            }
        }
        return null;

    }

    public PlayableTimeData GetPlayableTime(string clipName)
    {
        foreach (var item in TimeTrackDatas)
        {
            foreach (var clip in item.ClipsOfTrack)
            {
                if (clip.ClipDisplayName == clipName)
                {
                    return clip;
                }
            }
        }
        return null;

    }
}
