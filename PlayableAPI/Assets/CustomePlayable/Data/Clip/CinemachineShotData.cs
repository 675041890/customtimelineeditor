using System;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class CinemachineShotData : BaseClipData
{
    public string VirtualCameraName;
    public string FollowName;
    public string LookAtName;
    public int Priority;

    public override void SetAllData(TimelineClip clip, string attachtrack, PlayableDirector playDirector)
    {
        base.SetAllData(clip, attachtrack, playDirector);
        Type = PlayableAssetType.CinemachineShotPlayableAsset;

        var shot = clip.asset as CinemachineShot;
        var assetData = shot.VirtualCamera.Resolve(playDirector.playableGraph.GetResolver());

        VirtualCameraName = assetData.VirtualCameraGameObject.name;
        FollowName = assetData.Follow.name;
        LookAtName = assetData.LookAt.name;
        Priority = assetData.m_Priority;
    }

    public CinemachineShotData GetDerive(BaseClipData baseClipData)
    {
        return baseClipData as CinemachineShotData;
    }

}
