using System;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class EffectClipData : BaseClipData
{
    public string ParticleName;

    public string ParentName;

    public float px;
    public float py;
    public float pz;

    public float rx;
    public float ry;
    public float rz;

    public override void SetAllData(TimelineClip clip, string attachtrack, PlayableDirector playDirector)
    {
        base.SetAllData(clip, attachtrack, playDirector);
        Type = PlayableAssetType.EffectPlayableAsset;

        var assetData = clip.asset as EffectPlayableAsset; 
        ParticleName = assetData.EffectAsset.Particle.name;
        ParentName = assetData.Parent.Resolve(playDirector.playableGraph.GetResolver()).name;
        px = assetData.EffectAsset.Position.x;
        py = assetData.EffectAsset.Position.y;
        pz = assetData.EffectAsset.Position.z;
    }
}
