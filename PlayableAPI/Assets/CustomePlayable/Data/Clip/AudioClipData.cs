using System;
using UnityEngine.Playables;
using UnityEngine.Timeline;
[Serializable]
public class AudioClipData : BaseClipData
{
    public string AudioClipName;
    public bool IsLoop;
    public float Volume;

    public override void SetAllData(TimelineClip clip, string attachtrack, PlayableDirector playDirector = null)
    {
        base.SetAllData(clip, attachtrack);
        Type = PlayableAssetType.AudioPlayableAsset;
        
        var assetData = clip.asset as AudioPlayableAsset;
        AudioClipName = assetData.clip.name;
        IsLoop = assetData.loop;
        //GameObject audioSource = playDirector.GetGenericBinding(clip.parentTrack..o.outputs.GetEnumerator().Current.sourceObject) as GameObject;
        //var audio = audioSource.GetComponent<AudioSource>();
        //Volume = audio.volume;
    }
}
