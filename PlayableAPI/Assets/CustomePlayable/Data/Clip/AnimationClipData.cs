using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
[Serializable]
public class AnimationClipData : BaseClipData
{
    public string AnimationClipName;

    public override void SetAllData(TimelineClip clip, string attachtrack,PlayableDirector playableDirector = null)
    {
        base.SetAllData(clip, attachtrack);
        Type = PlayableAssetType.AnimationPlayableAsset;

        var assetData = clip.asset as AnimationPlayableAsset;
        AnimationClipName = assetData.clip.name;
    }
}
