using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class PlayableTimeData : BaseClipData
{
    public float Scale;

    public override void SetAllData(TimelineClip clip, string attachtrack, PlayableDirector playDirector)
    {
        base.SetAllData(clip, attachtrack, playDirector);
        Type = PlayableAssetType.PlayableTimeAsset;
        var assetData = clip.asset as TimePlayableAsset;
        Scale = assetData.TimeAsset.Scale;

    }
}
