using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
[Serializable]
public enum PlayableAssetType
{
    AnimationPlayableAsset,
    CinemachineShotPlayableAsset,
    EffectPlayableAsset,
    AudioPlayableAsset,
    PlayableTimeAsset
}
[Serializable]
public class BaseClipData : ISetClipAllData
{
    /// <summary>
    /// 该Clip所在的轨道名称
    /// </summary>
    public string AttachTrackName;

    /// <summary>
    /// 在Timeline上的片段名称
    /// </summary>
    public string ClipDisplayName;
    /// <summary>
    /// 该AnimationClip名称
    /// </summary>
    //public string AnimationClipName;
    /// <summary>
    /// clip开始时间
    /// </summary>
    public double StartTime;
    /// <summary>
    /// clip结束时间
    /// </summary>
    public double EndTime;
    /// <summary>
    /// 该Clip包含的目标附加数据的类型
    /// </summary>
    public PlayableAssetType Type;

    public virtual void SetAllData(TimelineClip clip, string attachtrack, PlayableDirector playDirector = null)
    {
        StartTime = clip.start;
        EndTime = clip.end;
        ClipDisplayName = clip.displayName;
        AttachTrackName = attachtrack;
    }

}

