using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

/// <summary>
/// 处理PlayableAsset相关逻辑,每个需要播放Timeline文件的都需要进行挂载，播放一个Timeline
/// 需要生成一个空节点（必须包含该脚本和PlayableDirector组件),
/// </summary>
[RequireComponent(typeof(PlayableDirector))]
public class TimelineBehaviour : MonoBehaviour
{
    private PlayableDirector playableDirector;
    private TimelineData timelineData;

    public void Awake()
    {
        playableDirector = this.GetComponent<PlayableDirector>();
        Init("Battle");
    }

    public void Init(string name)
    {
        // 动态加载playableAsset
        var tarAsset = Load<PlayableAsset>(name);
        playableDirector.playableAsset = tarAsset;

        // 加载timeline清单文件
        string path = string.Format("{0}.Manifest", name);
        //if (!File.Exists(path))
        //    return;
        timelineData = Load<TimelineAsset>(path).TimelineData;

        //读取TimelineData的轨道信息，进行绑定轨道,Camera,Audio
        foreach (var item in timelineData.CinemachineTrackDatas)
            SetTimelineBindings<Camera>(item.BaseTrackData.TrackName, item.BaseTrackData.TrackBindingName);
        foreach (var item in timelineData.AudioTrackDatas)
            SetTimelineBindings<AudioSource>(item.BaseTrackData.TrackName, item.BaseTrackData.TrackBindingName);

        foreach (var cinemachineTrack in timelineData.CinemachineTrackDatas) // 通过自定义数据修改已经加载的Timeline文件
        {
            foreach (var clip in cinemachineTrack.ClipsOfTrack)
            {
                SetTrack(clip.ClipDisplayName, clip);
            }
        }

        foreach (var audioTrack in timelineData.AudioTrackDatas) // 通过自定义数据修改已经加载的Timeline文件
        {
            foreach (var clip in audioTrack.ClipsOfTrack)
            {
                SetTrack(clip.ClipDisplayName, clip);
            }
        }

        foreach (var effectTrack in timelineData.EffectTrackDatas) // 通过自定义数据修改已经加载的Timeline文件
        {
            foreach (var clip in effectTrack.ClipsOfTrack)
            {
                SetTrack(clip.ClipDisplayName, clip);
            }
        }

        foreach (var timeTrack in timelineData.TimeTrackDatas) // 通过自定义数据修改已经加载的Timeline文件
        {
            foreach (var clip in timeTrack.ClipsOfTrack)
            {
                SetTrack(clip.ClipDisplayName, clip);
            }
        }
    }



    /// <summary>
    /// 修改命名shot的CinemachineShot内含数据为data
    /// </summary>
    /// <param name="clipName">被修改的clip名称</param>
    /// <param name="data">目标数据</param>
    //public void SetCinemachineShot(string clipName,CinemachineShotData data)
    //{
    //    var playableBindings = playableDirector.playableAsset.outputs;
    //    foreach (var binding in playableBindings)
    //    {
    //        var track = binding.sourceObject as TrackAsset;
    //        foreach (var clip in track.GetClips())
    //        {
    //            if (clip.displayName == clipName)
    //            {
    //                var go = SetCinemachineVirtualCamera(data);
    //                var shot = clip.asset as CinemachineShot;
    //                playableDirector.SetReferenceValue(shot.VirtualCamera.exposedName, go.GetComponent<CinemachineVirtualCamera>());
    //                return;
    //            }
    //        }
    //    }
    //}

    /// <summary>
    /// 设置track数据
    /// </summary>
    /// <typeparam name="T">track的clip数据类型</typeparam>
    /// <param name="clipName"></param>
    /// <param name="data"></param>
    public void SetTrack(string clipName, BaseClipData data) 
    {
        var playableBindings = playableDirector.playableAsset.outputs;
        foreach (var binding in playableBindings)
        {
            TrackAsset track = binding.sourceObject as TrackAsset;
            if (track == null) continue;
            foreach (var clip in track.GetClips())
            {
                if (clip.displayName == clipName) // clip的displayName 不能重名
                {
                    if (track.GetType() == typeof(CinemachineTrack))
                    {
                        var go = SetCinemachineVirtualCamera(data as CinemachineShotData);
                        var shot = clip.asset as CinemachineShot;
                        playableDirector.SetReferenceValue(shot.VirtualCamera.exposedName, go.GetComponent<CinemachineVirtualCamera>());
                        return;
                    }

                    if (track.GetType() == typeof(AudioTrack))
                    {
                        // 修改clip的值
                        ModifyAudioPlayableAsset(clip, data);
                        return;
                    }

                    if (track.GetType() == typeof(EffectTrack))
                    {

                        ModifyEffectPlayableAsset(clip, data);
                        return;
                    }

                    if(track.GetType() == typeof(TimeTrack))
                    {
                        ModifyTimePlayableAsset(clip, data);
                    }

                    return;
                }
            }

        }
    }

    public void ModifyTimePlayableAsset(TimelineClip clip, BaseClipData data)
    {
        var asset = clip.asset as TimePlayableAsset;
        var temp = data as PlayableTimeData;
        asset.TimeAsset.Scale = temp.Scale;
    }

    public void ModifyEffectPlayableAsset(TimelineClip clip, BaseClipData data)
    {
        var asset = clip.asset as EffectPlayableAsset;
        var temp = data as EffectClipData;
        asset.EffectAsset.Particle = Load<GameObject>(temp.ParticleName).GetComponent<ParticleSystem>();
        asset.EffectAsset.Parent = GameObject.Find(temp.ParentName).transform;
        playableDirector.SetReferenceValue(asset.Parent.exposedName, asset.EffectAsset.Parent);
        asset.EffectAsset.Particle.transform.localPosition = new Vector3(temp.px, temp.py, temp.pz);
        asset.EffectAsset.Particle.transform.localEulerAngles = new Vector3(temp.rx, temp.ry, temp.rz);

    }

    /// <summary>
    /// 修改AudioPlayableAsset的数据
    /// 将clip的数据修改为data
    /// </summary>
    /// <param name="clip"></param>
    /// <param name="data"></param>
    public void ModifyAudioPlayableAsset(TimelineClip clip, BaseClipData data)
    {
        // 修改clip的值
        var temp = data as AudioClipData;
        var audioAsset = clip.asset as AudioPlayableAsset;
        audioAsset.clip = Load<AudioClip>(temp.AudioClipName);
        audioAsset.loop = temp.IsLoop;
        //var audioSource = playableDirector.GetGenericBinding(binding.sourceObject) as AudioSource;
    }

    /// <summary>
    /// 设置CinemachineVirtualCamera数据
    /// </summary>
    /// <param name="data">要设置的数据</param>
    /// <returns></returns>
    public GameObject SetCinemachineVirtualCamera(CinemachineShotData data)
    {
        var go = InstantiateTarget<CinemachineVirtualCamera>(data.VirtualCameraName);
        var goData = go.GetComponent<CinemachineVirtualCamera>();
        goData.Follow = GameObject.Find(data.FollowName).transform;
        goData.LookAt = GameObject.Find(data.LookAtName).transform;
        return go;
    }

    public GameObject InstantiateTarget<T>(string name) where T : Component
    {
        var go = GameObject.Find(name);
        if (go != null) return go;
        go = GameObject.Instantiate(new GameObject());
        go.name = name;
        go.AddComponent<T>();
        return go;
    }

    public T Load<T>(string name) where T : UnityEngine.Object
    {
        T t = Resources.Load<T>(name);
        return t;
    }

    /// <summary>
    /// 设置PlayableAsset.Bindings,加载PlayableAsset,可能轨道的绑定对象要更换，比如相机的运镜：原来cam1,换一个场景，同样的运镜
    /// 需要基于cam3,在需要播放该PlayableAsset时，需要从新指定clip的内容，可以是读表，也可以是根据运行时的需要，比如当前相机运镜
    /// 看向目标A，我想要该运镜模式不变，只是改变该运镜的目标,所有的改变都必须在加载该PlayableAsset时进行改变，不能在其播放进行时改变
    /// </summary>
    /// <param name="track">轨道名称</param>
    /// <param name="go">绑定的游戏对象名称</param>
    public void SetTimelineBindings<T>(string track, string target) where T : Component
    {
        var playableBindings = playableDirector.playableAsset.outputs;
        foreach (var binding in playableBindings)
        {
            if (binding.streamName.Equals(track))
            {
                var go = GetTarget<T>(target);
                playableDirector.SetGenericBinding(binding.sourceObject, go);
                return;
            }
        }
    }

    private GameObject GetTarget<T>(string name) where T : Component
    {
        var go = GameObject.Find(name);
        if (go != null) return go;
        go = GameObject.Instantiate(new GameObject());
        go.name = name;
        go.AddComponent<T>();
        return go;
    }
}
