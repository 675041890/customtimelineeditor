using Cinemachine;
using System;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.Playables;

/// <summary>
/// 导出轨道的绑定数据，轨道的所有clip;
/// </summary>
public class ExportPlayableData
{
    public const string TIMELINE_CAMERATRACK = "Cinemachine"; 
    public const string TIMELINE_AUDIOTRACK = "Audio"; 
    public const string TIMELINE_EFFECTTRACK = "Effect"; 
    public const string TIMELINE_TIMETRACK = "Time"; 
    public static TimelineData TimelineData = new TimelineData();
    public static PlayableDirector PlayableDirector;

    private static void createSaveScriptObj()
    {
        string path0 = string.Format("Assets/BindingAssets/{0}.asset", PlayableDirector.playableAsset.name);

        //如果已经存在有binding的绑定，则决定是否替换或者保留
        if (File.Exists(path0))
        {
            if (EditorUtility.DisplayDialog("注意", "替换原来的binding？", "是", "否"))
            {
                File.Delete(path0);
            }
            else
                return;
        }

        //实例化类
        TimelineAsset bindingAsset = ScriptableObject.CreateInstance<TimelineAsset>();

        //赋初值
        bindingAsset.TimelineData = TimelineData;
        //如果实例化为空，返回
        if (!bindingAsset)
        {
            Debug.LogWarning("bindingAsset not found");
            return;
        }
        //自定义资源保存路径
        string path = Application.dataPath + "/BindingAssets";

        //如果项目不包含该路径，创建一个
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }

        path = string.Format("Assets/BindingAssets/{0}.asset", PlayableDirector.playableAsset.name);

        //生成自定义资源到指定路径
        AssetDatabase.CreateAsset(bindingAsset, path);
    }

    [MenuItem("TimelineAsset/Save")]
    public static void SaveTimelineData()
    {
        PlayableDirector = Selection.activeGameObject.GetComponent<PlayableDirector>();
        foreach (var at in PlayableDirector.playableAsset.outputs)
        {
            Type t;
            if (at.sourceObject != null)
            {
                t = at.sourceObject.GetType();
                TimelineData.SetTimelineData(at, PlayableDirector, t);
            }               
        }

        createSaveScriptObj();
    }

}



