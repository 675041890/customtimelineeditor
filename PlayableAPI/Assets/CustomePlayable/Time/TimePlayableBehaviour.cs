using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

[Serializable]
public class TimePlayableBehaviour : PlayableBehaviour
{
    public float Scale;
    private float curTime;
    //private float startTime;
    //private float endTime;

    public override void OnGraphStop(Playable playable)
    {
        Time.timeScale = 1;
    }

    public override void OnBehaviourPlay(Playable playable, FrameData info)
    {
        Time.timeScale = Scale;
    }

    public override void OnBehaviourPause(Playable playable, FrameData info)
    {
        Time.timeScale = 1;
    }

    public override void PrepareFrame(Playable playable, FrameData info)
    {
        //curTime += info.deltaTime;
    }

    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {

    }
}
