using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[System.Serializable]
public class TimePlayableAsset : PlayableAsset, ITimelineClipAsset
{
    public ClipCaps clipCaps { get; set; }

    public TimePlayableBehaviour TimeAsset; 

    public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<TimePlayableBehaviour>.Create(graph, TimeAsset);
        return playable;
    }

}
