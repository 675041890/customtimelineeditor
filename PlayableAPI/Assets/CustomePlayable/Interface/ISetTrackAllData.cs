using UnityEngine.Playables;

public interface ISetTrackAllData
{
    void SetAllData(PlayableBinding playableBinding, PlayableDirector playDirector);
}

