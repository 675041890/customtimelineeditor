using UnityEngine.Playables;
using UnityEngine.Timeline;

public interface ISetClipAllData
{
    void SetAllData(TimelineClip clip, string attachtrack, PlayableDirector playDirector);
}
